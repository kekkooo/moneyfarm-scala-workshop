package org.talk.service

import java.time.LocalDateTime

import org.talk.db.{BankAccount, Customer, Operation}
import org.talk.db.dao.{CustomerDao, BankAccountDao, OperationDao}
import org.talk.protocol.BankAccountDetails

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class BankService(
                   private val customerDao: CustomerDao,
                   private val bankAccountDao: BankAccountDao,
                   private val operationDao: OperationDao
                 ) {
  def cashTransfer(bankAccountId: Long, amount: BigDecimal): Future[Long] = ???

  def createBankAccount(name: String, customerId: Long) = ???

  def getBankAccountDetails(id: Long, atDate: LocalDateTime = LocalDateTime.now): Option[BankAccountDetails] = {
    for {
      account <- bankAccountDao.get(id)
      customer <- customerDao.get(account.customerId)
      operations <- operationDao.getForBankAccount(id, atDate)
    } yield buildBankAccountDetails(account, customer, operations)

  }

  def getCustomersBankAccounts(customerId: Long): Future[Option[Iterable[Option[BankAccountDetails]]]] = Future {
    val bankAccounts = bankAccountDao.getForCustomer(customerId)
    customerDao.get(customerId).flatMap { customer =>
      bankAccounts.map { accounts =>
        accounts.map { account =>
          operationDao.getForBankAccount(account.id).map { operations =>
            buildBankAccountDetails(account, customer, operations)
          }
        }
      }
    }
  }

  private def getOperationsSum(operations: Iterable[Operation]): BigDecimal = operations.map(_.amount).sum


  private def buildBankAccountDetails(bankAccount: BankAccount, customer: Customer, operations: Iterable[Operation]) = {
    val customerName = s"${customer.firstName} ${customer.lastName}"
    val balance = getOperationsSum(operations)
    BankAccountDetails(bankAccount.id, bankAccount.accountName, customerName, balance)
  }

}
