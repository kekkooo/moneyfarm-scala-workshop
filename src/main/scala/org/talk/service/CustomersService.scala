package org.talk.service

import org.talk.db.dao.CustomerDao
import org.talk.db.Customer
import org.talk.protocol.{CustomerSummary, RegisterCustomerRequest}

class CustomersService(private val customerDao: CustomerDao) {

  def registerCustomer(registerCustomerRequest: RegisterCustomerRequest): Long =
    customerDao.insert(
      Customer(
        firstName = registerCustomerRequest.firstName,
        lastName = registerCustomerRequest.lastName,
        birthday = registerCustomerRequest.birthday )
    )

  def getAllCustomers(): Iterable[CustomerSummary] =
    customerDao.getAll().map { customer =>
      CustomerSummary(customer.firstName, customer.lastName, customer.birthday)
    }

  def getCustomer(id: Long): Option[CustomerSummary] = customerDao.get(id).map(customerToSummary)

  private def customerToSummary(customer: Customer) = CustomerSummary(customer.firstName, customer.lastName, customer.birthday)

}
