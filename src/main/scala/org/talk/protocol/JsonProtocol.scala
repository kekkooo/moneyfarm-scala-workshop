package org.talk.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.json._

trait JsonProtocol extends SprayJsonSupport with DateTimeJsonSupport with DefaultJsonProtocol{

  implicit val operationFormat = jsonFormat3(Operation)
  implicit val customerFormat = jsonFormat3(CustomerSummary)

  implicit val bankAccountDetailsFormat = jsonFormat4(BankAccountDetails)

  implicit val createBankAccountRequestFormat = jsonFormat1(CreateBankAccountRequest)
  implicit val registerCustomerRequestFormat = jsonFormat3(RegisterCustomerRequest)
  implicit val aggregateCustomerBankAccountDetailFormat = jsonFormat3(AggregateCustomerBankAccountsDetail)

}
