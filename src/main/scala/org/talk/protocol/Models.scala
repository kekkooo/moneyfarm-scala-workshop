package org.talk.protocol

import java.time.{LocalDateTime, LocalDate}

case class CustomerSummary(
                     firstNme: String,
                     lastName: String,
                     birthday: LocalDate)

case class Operation(id: Long, cents: Long, timeStamp: LocalDateTime){

  def isDeposit: Boolean = cents >= 0
  def isWithdrowal: Boolean = !isDeposit
}

case class BankAccountDetails(
                               bankAccountId: Long,
                               bankAccountName: String,
                               customerFulName: String,
                               balance: BigDecimal)

case class AggregateCustomerBankAccountsDetail(
                                                 customerId: Long,
                                                 bankAccountNames: List[String],
                                                 aggregateBalance: BigDecimal )

case class CreateBankAccountRequest(bankAccountName: String)
case class RegisterCustomerRequest(firstName: String, lastName: String, birthday: LocalDate)