package org.talk.protocol

import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter._

import spray.json.{DeserializationException, JsValue, JsString, RootJsonFormat}

trait DateTimeJsonSupport {
  implicit object LocalDateTimeJsonFormat extends RootJsonFormat[LocalDateTime] {
    override def write(obj: LocalDateTime) = JsString(obj.format(ISO_LOCAL_DATE_TIME))

    override def read(json: JsValue) : LocalDateTime = json match {
      case JsString(s) => LocalDateTime.parse(s, ISO_LOCAL_DATE_TIME)
      case _ => throw new DeserializationException(s"Unable to deserialize $json")
    }
  }

  implicit object LocalDateJsonFormat extends RootJsonFormat[LocalDate] {
    override def write(obj: LocalDate) = JsString(obj.format(ISO_LOCAL_DATE))

    override def read(json: JsValue) : LocalDate = json match {
      case JsString(s) => LocalDate.parse(s, ISO_LOCAL_DATE)
      case _ => throw new DeserializationException(s"Unable to deserialize $json")
    }
  }
}
