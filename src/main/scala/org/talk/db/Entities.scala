package org.talk.db

import java.time.{LocalDate, LocalDateTime}

trait Entity{
  def id: Long
  def withId(newId: Long): Entity
}

case class Customer(
  override val id: Long = 0,
  firstName: String,
  lastName: String,
  birthday: LocalDate ) extends Entity{
  override def withId(newId: Long): Customer = this.copy(id = newId)
}

case class BankAccount(
  id: Long = 0,
  accountName: String,
  customerId: Long)extends Entity{
  override def withId(newId: Long): BankAccount = this.copy(id = newId)
}

case class Operation(
  id: Long = 0,
  amount: BigDecimal,
  createdOn: LocalDateTime = LocalDateTime.now,
  bankAccountId: Long )extends Entity{
  override def withId(newId: Long): Operation = this.copy(id = newId)
}