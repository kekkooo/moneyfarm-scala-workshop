package org.talk.db.dao

import org.talk.db.{Customer, Entity}

class CustomerDao extends InMemoryStorage[Customer]

trait InMemoryStorage[A <: Entity] {
  protected val items: collection.mutable.Map[Long, A] = collection.mutable.Map.empty

  def insert(entity: A): Long = {
    val freshId = if (items.isEmpty) 1 else items.keys.max + 1
    val e = entity.withId(freshId).asInstanceOf[A]
    items += (freshId -> e)
    freshId
  }

  def getAll(): Iterable[A] = items.values

  def get(id: Long): Option[A] = items.get( id )

  def delete(id: Long) = items -= id

  def update(id: Long, entity: A) = items += (id -> entity)
}