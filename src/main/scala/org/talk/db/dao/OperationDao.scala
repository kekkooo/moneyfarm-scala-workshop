package org.talk.db.dao

import java.time.LocalDateTime

import org.talk.db.Operation

class OperationDao(bankAccountDao: BankAccountDao) extends InMemoryStorage[Operation]{

  def getForBankAccount( bankAccountId: Long, limit: LocalDateTime = LocalDateTime.now): Option[Iterable[Operation]] = for {
    _ <- bankAccountDao.get(bankAccountId)
  } yield this.items.values.filter(_.bankAccountId == bankAccountId)

}

