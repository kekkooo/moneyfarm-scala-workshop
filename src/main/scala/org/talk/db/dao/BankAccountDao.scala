package org.talk.db.dao

import org.talk.db.BankAccount

class BankAccountDao(customerDao: CustomerDao) extends InMemoryStorage[BankAccount] {
  def getForCustomer(customerId: Long): Option[Iterable[BankAccount]] = for {
    _ <- customerDao.get(customerId)
  } yield items.values.filter(bankAccount => bankAccount.customerId == customerId)

  def addToCustomer(customerId: Long, accountName: String): Option[Long] =
    for {
      _ <- customerDao.get(customerId)
    } yield this.insert(BankAccount(accountName = accountName, customerId = customerId ))

}