package org.talk

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import org.talk.db.dao.{BankAccountDao, CustomerDao, OperationDao}
import org.talk.protocol.{CreateBankAccountRequest, JsonProtocol, RegisterCustomerRequest}
import org.talk.service.{BankService, CustomersService}


 class Routes extends JsonProtocol with LazyLogging {

  private val customerDao = new CustomerDao
  private val bankAccountDao = new BankAccountDao(customerDao)
  private val operationDao = new OperationDao(bankAccountDao)

  val customerService = new CustomersService(customerDao)
  val bankService = new BankService(customerDao, bankAccountDao, operationDao)


  val routes = customerRoutes ~ bankAccountRoutes

  def customerRoutes =
    pathPrefix( "customers" ) {
      path( LongNumber ) { customerId =>
        get {
          rejectEmptyResponse {
            complete( customerService.getCustomer( customerId ) )
          }
          //          val maybeCustomer = customerService.getCustomer( customerId )
          //          maybeCustomer match {
          //            case None => complete( NotFound -> s"Customer $customerId has not been found" )
          //            case Some( customer ) => complete( OK -> customer )
          //          }
        }
      } ~
        pathEnd {
          get {
            complete( customerService.getAllCustomers() )
          } ~
            post {
              entity( as[RegisterCustomerRequest] ) { request =>
                complete (Created -> customerService.registerCustomer( request ).toString)
              }
            }
        }
    }

  def bankAccountRoutes =
    pathPrefix( "customers" / LongNumber / "bankAccounts" ) { customerId =>
      pathEnd {
        get {
          rejectEmptyResponse {
            complete {
              bankService.getCustomersBankAccounts( customerId )
            }
          }
        } ~ post {
          entity( as[CreateBankAccountRequest] ) { request =>
            complete(
              Created -> bankService.createBankAccount( request.bankAccountName, customerId ).toString
            )
          }
        }
      }
    }
}