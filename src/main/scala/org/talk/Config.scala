package org.talk

import com.typesafe.config.ConfigFactory

object  Config {
  private lazy val config = ConfigFactory.load("application.conf")
  // http configuration
  lazy val HostName = config.getString("http.hostname")
  lazy val Port = config.getInt("http.port")
  lazy val RequestTimeout = config.getInt("http.timeout")

}
