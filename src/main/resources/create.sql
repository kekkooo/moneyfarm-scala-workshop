CREATE SCHEMA IF NOT EXISTS bank;
USE bank;

CREATE TABLE IF NOT EXISTS customer(
  id INT NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  birthday DATE NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS bank_account(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  account_name VARCHAR(100),
  customer_id INT NOT NULL,
  CONSTRAINT fk_bank_account_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE TABLE IF NOT EXISTS opeartion(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  amount BIGINT NOT NULL,
  created_on DATETIME NOT NULL,
  bank_account_id INT NOT NULL,
  CONSTRAINT fk_operation_bank_account_id FOREIGN KEY (bank_account_id) REFERENCES bank_account(id)
);